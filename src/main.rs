extern crate rand;

use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use rand::Rng;

fn get_file() -> Vec<String> {
    loop {
        println!("Enter the file you want to use: ");
        let mut file = String::new();
        io::stdin()
            .read_line(&mut file)
            .expect("Failed to read line, WHAT THE FUCK!");

        match get_words(file.trim()) {
            Ok(words) => return words,
            Err(_) => {
                println!("Could not read file!");
            }
        }
    }
}

fn get_words(file: &str) -> std::io::Result<Vec<String>> {
    let file = File::open("lists/".to_owned() + file)?;
    let reader = BufReader::new(file);
    let mut contents = Vec::<String>::new();
    for line in reader.lines() {
        contents.push(line.unwrap().trim().to_string());
    }

    Ok(contents)
}

fn guess_words(words: Vec<String>) {
    let start_time = std::time::SystemTime::now();
    let mut corr = 0;

    let tries = get_tries();

    println!("Write the word!");
    for i in 0..tries {
        let mut guess = String::new();

        let word_nr = rand::thread_rng().gen_range(0, words.len());
        let word = &words[word_nr];
        println!("{}. {}", i + 1, word);

        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line, WHAT THE FUCK!");

        guess = guess.trim().to_string();

        if guess == *words[word_nr] {
            println!("(｡◕‿◕｡) Correct!");
            corr = corr + 1;
        } else {
            println!("(ಠ_ಠ) Wrong!");
        }
    }
    let end_time = std::time::SystemTime::now()
        .duration_since(start_time)
        .expect("");
    println!(
        "You guessed {} words correctly in {} Seconds! That's {} seconds per word on average!",
        corr,
        end_time.as_secs(),
        end_time.as_secs() as u32 / tries
    );
}

fn get_tries() -> u32 {
    let mut number = String::new();
    loop {
        println!("Enter the number of words you want:");
        io::stdin()
            .read_line(&mut number)
            .expect("Failed to read line, WHAT THE FUCK!");

        let number = match number.trim().parse::<u32>() {
            Ok(num) => num,
            Err(_) => continue,
        };
        match number {
           0 => return 1,
           _ => return number,
        };
    }
}

fn main() {
    let words = get_file();
    guess_words(words);
}
