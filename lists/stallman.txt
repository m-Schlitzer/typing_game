Sharing is good, and with digital technology, sharing is easy.
Android is a major step towards an ethical, user-controlled, free-software portable phone, but there is a long way to go.
Facebook is not your friend, it is a surveillance engine.
If you want to accomplish something in the world, idealism is not enough - you need to choose a method that works to achieve the goal.
Free software is software that respects your freedom and the social solidarity of your community. So it's free as in freedom.
Value your freedom or you will lose it, teaches history. 'Don't bother us with politics', respond those who don't want to learn.
The desire to be rewarded for one's creativity does not justify depriving the world in general of all or part of that creativity.
Anything that prevents you from being friendly, a good neighbour, is a terror tactic.
All governments should be pressured to correct their abuses of human rights.
The reason that a good citizen does not use such destructive means to become wealthier is that, if everyone did so, we would all become poorer from the mutual destructiveness.
If you use a proprietary program or somebody else's web server, you're defenceless. You're putty in the hands of whoever developed that software.
Control over the use of one's ideas really constitutes control over other people's lives; and it is usually used to make their lives more difficult.
In the free/libre software movement, we develop software that respects users' freedom, so we and you can escape from software that doesn't.
There is nothing wrong with wanting pay for work, or seeking to maximize one's income, as long as one does not use means that are destructive.
