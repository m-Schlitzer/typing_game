Frankly, my dear, I don't give a damn. - Gone with the wind
I'm gonna make him an offer he can't refuse. - The Godfather
You don't understand! I coulda had class. I coulda been a contender. I could've been somebody, instead of a bum, which is what I am. - On the waterfront
You talking to me? - Taxi Driver
I love the smell of napalm in the morning.  - Apocalypse Now
I'm as mad as hell, and I'm not going to take this anymore! - Network
A census taker once tried to test me. I ate his liver with some fava beans and a nice Chianti. - The silence of the lambs
There's no place like home.  - The Wizard of Oz
Round up the usual suspects. - Casablanca
You're gonna need a bigger boat. - Jaws
My mama always said life was like a box of chocolates. You never know what you're gonna get. - Forrest Gump
We rob banks. - Bonnie and Clyde
A boy's best friend is his mother. - Psycho
Take your stinking paws off me, you damned dirty ape. - Planet of the apes
Soylent Green is people! - Soylent green
Open the pod bay doors, please, HAL. - 2001: A space odyssey
Listen to them. Children of the night. What music they make. - Dracula
Life is a banquet, and most poor suckers are starving to death! - Auntie Mame
I feel the need - the need for speed! - Top Gun
Carpe diem. Seize the day, boys. Make your lives extraordinary. - Dead poets society
